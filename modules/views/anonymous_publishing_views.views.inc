<?php

/**
 * @file
 * Provides integration with the Views module.
 *
 * Tells the Views module about the Anonymous Publishing database to
 * include the alias field in views.
 */

/**
 * Implements hook_views_query_alter().
 */
function anonymous_publishing_views_views_query_alter(&$view, &$query) {
  foreach ($query->fields as $field_key => &$field_values) {
    if ($field_values['table'] == 'anonymous_publishing') {
      switch ($field_values['field']) {
        case 'byline':
          unset($query->fields[$field_key]);
          $query->add_field(NULL, "(SELECT alias FROM {anonymous_publishing_emails} WHERE email = anonymous_publishing.email)", $field_key);
          break;
      }
    }
  }
}

/**
 * Implements hook_views_data().
 */
function anonymous_publishing_views_views_data() {
  $data = array();
  $data['anonymous_publishing']['table']['group'] = t('Anonymous Publishing');
  $data['anonymous_publishing']['table']['base'] = array(
    // This is the identifier field for the view.
    'field' => 'nid',
    'title' => t('Anonymous Publishing'),
    'help' => t('Table contains ??? and can be related to nodes.'),
    'weight' => -10,
  );

  // This table references the {node} table. The declaration below creates an
  // 'implicit' relationship to the node table, so that when 'node' is the base
  // table, the fields are automatically available.
  $data['anonymous_publishing']['table']['join'] = array(
    // The node table:
    'node' => array(
      // Primary key:
      'left_field' => 'nid',
      // Foreign key:
      'field' => 'nid',
    ),
  );

  $data['anonymous_publishing']['nid'] = array(
    'title' => t('Content published anonymously'),
    'help' => t('Content referencing a node published anonymously.'),
  );

  // Join with the email field.
  $data['anonymous_publishing']['email'] = array(
    'title' => t('Email'),
    'help' => t('Email used to activate.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is used by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Set up a dummy byline field to be populated via a subquery.
  // https://drupal.stackexchange.com/a/28453/12076
  $data['anonymous_publishing']['byline'] = array(
    'title' => t('Byline'),
    'help' => t('Byline picked by user.'),
    'field' => array(
      'handler' => 'views_handler_field',
      // This is used by the table display plugin.
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}
