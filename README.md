# CONTENTS OF THIS FILE

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers


# INTRODUCTION

The **Anonymous Publishing** module increases your control over anonymous
publishing on a site.

* For a description of the module, visit the [project page][1].

* To submit bug reports and feature suggestions, or to track changes
  visit the project's [issue tracker][2].

* For documentation, visit the [documentation page][3], or enable
 [Advanced Help][4].
  
**Anonymous Publishing** may lower the threshold for authorship and entry
to a site.  It may also be a requirement for certain sites that deal
with sensitive issues that anonymous publishing is allowed.

# REQUIREMENTS

None.

# RECOMMENDED MODULES

* [Advanced Help Hint][5]:  
  Links help text provided by `hook_help` to online help and
  **Advanced Help**.
* [Advanced Help][4]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/anonymous_publishing/README.md`.
* [Markdown][6]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered with the markdown filter.


# INSTALLATION

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][7] for further information.

2. Enable the **Anonymous Publishing** module on the *Modules* list
   page.  The database tables will be created automagically for you at
   this point.

3. Check the *Status report* to see if it necessary to run the
   database update script.

4. Enable the submodules providing the features you want to use.

5. Proceed to configure each of the submodules, as described in the
   configuraton section below.


# CONFIGURATION

By itself, the parent module does nothing beyond creating and dropping
the database tables used by this project. You need to enable at least
one of the project's submodules to be able to use the features
provided by this project.

The forms to configure the **Anonymous Publishing CL** and **Anonymous
publishing PET** submodules are found by navigating to:

    Admin » Configuration » People

**Anonymous Publishing LR** does not have a configuration form.

For detailed configuration instructions for each of the submodules,
visit the [online documentation page][3], or enable
[Advanced Help][4].


# MAINTAINERS

**Anonymous Publishing** was first created by [dropcube][8] (Ronny
López).  
The current maintainer is [gisle][9] (Gisle Hannemyr).

Any help with development (patches, reviews, comments) are welcome.

Development has been sponsored by [Hannemyr Nye Medier AS][10].

[1]: https://drupal.org/project/anonymous_publishing
[2]: https://drupal.org/project/issues/anonymous_publishing
[3]: https://www.drupal.org/docs/7/modules/anonymous-publishing
[4]: https://www.drupal.org/project/advanced_help
[5]: https://www.drupal.org/project/advanced_help_hint
[6]: https://www.drupal.org/project/markdown
[7]: https://www.drupal.org/docs/7/extend/installing-modules
[8]: https://www.drupal.org/user/37031
[9]: https://www.drupal.org/u/gisle
[10]: https://hannemyr.no
